=========================
Stock Price List Scenario
=========================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install stock_price_list::

    >>> config = activate_modules('stock_price_list')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer_without_price_list = Party(name='Customer without price list')
    >>> customer_without_price_list.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_expense = expense
    >>> template.account_revenue = revenue
    >>> template.save()
    >>> product, = template.products

Create a price List and assign it to customer::

    >>> PriceList = Model.get('product.price_list')
    >>> price_list = PriceList(name='Retail')

    >>> price_list_line = price_list.lines.new()
    >>> price_list_line.quantity = 10.0
    >>> price_list_line.product = product
    >>> price_list_line.formula = 'unit_price * 0.5'

    >>> price_list.save()

    >>> customer.sale_price_list = price_list
    >>> customer.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

    >>> StockMove = Model.get('stock.move')
    >>> shipment_out.outgoing_moves.extend([StockMove()])
    >>> for move in shipment_out.outgoing_moves:
    ...     move.from_location = output_loc
    ...     move.to_location = customer_loc
    ...     move.product = product
    ...     move.uom = unit
    ...     move.quantity = 1
    ...     move.company = company
    ...     move.currency = company.currency
    >>> shipment_out.save()
    >>> move = shipment_out.outgoing_moves[0]

When quantity is 1 move has product's list price::

    >>> move.unit_price
    Decimal('10')

Discount applied when quantity is 10::

    >>> move.quantity = 10
    >>> move.unit_price
    Decimal('5.0000')