# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from decimal import Decimal

__all__ = ['Move']


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    def _get_context_sale_price(self):
        context = {}
        if self.shipment.customer:
            context['customer'] = self.shipment.customer.id
        if self.shipment.customer.sale_price_list:
            # TODO: should be selected by user
            context['price_list'] = self.shipment.customer.sale_price_list.id
        if self.uom:
            context['uom'] = self.uom.id
        return context

    @fields.depends('product', 'quantity', 'uom', 'unit', 'unit_price',
        'taxes', 'shipment', '_parent_shipment.customer', 'to_location',
        'from_location', 'origin')
    def on_change_quantity(self):
        Product = Pool().get('product.product')

        def customer_move():
            return (
                self.shipment.__name__ == 'stock.shipment.out' and
                self.to_location and
                self.to_location.type == 'customer'
                ) or (
                self.shipment.__name__ == 'stock.shipment.out.return' and
                self.from_location and
                self.from_location.type == 'customer')

        if (self.shipment and self.product and customer_move() and
                not self.origin):
            with Transaction().set_context(
                    self._get_context_sale_price()):
                self.unit_price = Product.get_sale_price([self.product],
                    self.quantity or 0)[self.product.id]
                if self.unit_price:
                    self.unit_price = self.unit_price.quantize(
                        Decimal(1) / 10 ** self.__class__.unit_price.digits[1])
        elif hasattr(super(Move, self), 'on_change_quantity'):
            super(Move, self).on_change_quantity()
